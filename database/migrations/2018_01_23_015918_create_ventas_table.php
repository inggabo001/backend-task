<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVentasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ventas', function (Blueprint $table) {
            $table->integer('factura_numero')->unsigned();
            $table->string('producto_codigo',20)->unsigned();
            $table->integer('cantidad');

            $table->primary(['factura_numero', 'producto_codigo']);

            $table->foreign('factura_numero')
                ->references('numero')->on('facturas')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('producto_codigo')
                ->references('codigo')->on('productos')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ventas');
    }
}
