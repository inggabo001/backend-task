<?php

use Illuminate\Database\Seeder;
use App\Models\Cliente;

class ClientesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // creamos un cliente de ejemplo
        $clientes = 
        [
        	[
        		'cedula'        =>	123456789,
                'razon-social'  =>	'Empresa de Pruebas C.A',
        		'direccion'		    =>	'Avenida Principal',
        		'telefono'		    => '0212-1234567',
        	]
        ];

        foreach ($clientes as $cliente) {
        	$crear = Cliente::create($cliente);
        }
    }
}
