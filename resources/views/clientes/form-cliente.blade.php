<!-- extendemos a la plantilla principal donde se encuentra el menu-->
@extends('layouts.main')

<!-- titulo de la pagina -->
@section('title','Home')

<!-- titulo en el contenido -->
@section('tit')
  <h2>Registrar Cliente</h2>
@endsection

<!-- seccion para mostrar errores de validacion de formularios -->
@section('errors')
 @if ($errors->any())
      <div class="alert alert-danger">
          <ul>
              @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
              @endforeach
          </ul>
      </div>
  @endif
@endsection

<!-- contenido -->
@section('content')
 <!-- formulario para crear clientes con la ruta clientes.store definida en la ruta resources -->
 
<form action="{{ route('clientes.store') }}" method="POST">
    <input type="hidden" name="_method" value="POST">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">

    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
        <input class="mdl-textfield__input" type="text" pattern="-?[0-9]*(\.[0-9]+)?" id="cedula" name="cedula">
        <label class="mdl-textfield__label" for="cedula">Cedula</label>
        <span class="mdl-textfield__error">El dato no es un numero!</span>
    </div>
    <br>
    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
        <input class="mdl-textfield__input" type="text" id="razon_social" name="razon_social">
        <label class="mdl-textfield__label" for="razon_social">Razon Social</label>
    </div>
    <br>
    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
        <input class="mdl-textfield__input" type="text" id="telefono" name="telefono">
        <label class="mdl-textfield__label" for="telefono">Telefono</label>
    </div>
    <br>
    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
        <textarea class="mdl-textfield__input" type="text" rows="1" id="direccion" name="direccion"></textarea>
        <label class="mdl-textfield__label" for="direccion">Direccion</label>
    </div>
    <br>

    <button type="submit" value="Registrar" class="mdl-button mdl-js-button mdl-button--raised" name="Registrar">Registrar</button>

</form>
@endsection
