<!-- extendemos a la plantilla principal donde se encuentra el menu-->
@extends('layouts.main')

<!-- titulo de la pagina -->
@section('title','Editar Cliente')

<!-- titulo en el contenido -->
@section('tit')
  <h2>Editar Cliente</h2>
@endsection

<!-- seccion para mostrar errores de validacion de formularios -->
@section('errors')
 @if ($errors->any())
      <div class="alert alert-danger">
          <ul>
              @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
              @endforeach
          </ul>
      </div>
  @endif
@endsection

@section('content')
<form action="{{ route('clientes.update', $cliente['cedula'] ) }}" method="POST">

    <input type="hidden" name="_method" value="PUT">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">

    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
        <input class="mdl-textfield__input" type="text" pattern="-?[0-9]*(\.[0-9]+)?" id="cedula" name="cedula" value="{{ $cliente['cedula'] }}">
        <label class="mdl-textfield__label" for="cedula">Cedula</label>
        <span class="mdl-textfield__error">El dato no es un numero!</span>
    </div>
    <br>
    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
        <input class="mdl-textfield__input" type="text" id="rz-social" name="razon_social" value="{{ $cliente['razon_social'] }}">
        <label class="mdl-textfield__label" for="rz-social">Razon Social</label>
    </div>
    <br>
    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
        <input class="mdl-textfield__input" type="text" id="telefono" name="telefono" value="{{ $cliente['telefono'] }}">
        <label class="mdl-textfield__label" for="telefono">Telefono</label>
    </div>
    <br>
    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
        <textarea class="mdl-textfield__input" type="text" rows="1" id="direccion" name="direccion">{{ $cliente['direccion'] }}</textarea>
        <label class="mdl-textfield__label" for="direccion">Direccion</label>
    </div>
    <br>

    <button type="submit" value="Modificar" class="mdl-button mdl-js-button mdl-button--raised" name="Modificar">Modificar</button>
    
</form>
@endsection