@extends('layouts.main')

@section('title','Clientes')

<!-- titulo en el contenido -->
@section('tit')
  <h2>Clientes</h2>
@endsection

@section('agregar')
<div style="float: right; margin-bottom: 20px;">
  <a href="{{ route('clientes.create')}} ">
    <button class="mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect">
    <i class="material-icons">add</i>  
    </button>
  </a>
</div>
@endsection

@section('content')

<table class="mdl-data-table mdl-js-data-table mdl-shadow--2dp">
  <thead>
    <tr>
      <th>Cedula</th>
      <th class="mdl-data-table__cell--non-numeric">Razon Social</th>
      <th class="mdl-data-table__cell--non-numeric">Direccion</th>
      <th class="mdl-data-table__cell--non-numeric">Telefono</th>
      <th class="mdl-data-table__cell--non-numeric">Accion</th>
    </tr>
  </thead>
  <tbody>
    <!-- si la tabla clientes no esta vacia hacemos un recorrido
        y mostramos todos los datos -->
    @if($clientes != '')
      @foreach($clientes as $cliente )
      <tr>
        <td>{{ $cliente['cedula'] }}</td>
        <td class="mdl-data-table__cell--non-numeric">{{ $cliente['razon_social'] }}</td>
        <td class="mdl-data-table__cell--non-numeric">{{ $cliente['direccion'] }}</td>
        <td class="mdl-data-table__cell--non-numeric">{{ $cliente['telefono'] }}</td>
        <td class="mdl-data-table__cell--non-numeric">
          <a href="clientes/{{ $cliente['cedula'] }}/edit">
            <button class="mdl-button mdl-js-button mdl-button--icon">
              <i class="material-icons">mode_edit</i>
            </button>
          </a>
          
            <a href="{{ route('clientes.destroy', $cliente['cedula']) }}">
              <button class="mdl-button mdl-js-button mdl-button--icon">
                <i class="material-icons">delete</i>
              </button>
          </a>
        </td>

      </tr>
      @endforeach
    @endif
    
  </tbody>
</table>
@endsection