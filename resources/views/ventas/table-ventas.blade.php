@extends('layouts.main')

@section('title','Ventas')
<!-- titulo en el contenido -->
@section('tit')
  <h2>Ventas</h2>
@endsection

@section('agregar')
  <div style="float: right; margin-bottom: 20px;">
    <a href="{{ route('ventas.create')}} ">
      <button class="mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect">
      <i class="material-icons">add</i>  
      </button>
    </a>
  </div>
@endsection

@section('content')

<table class="mdl-data-table mdl-js-data-table mdl-shadow--2dp" style="margin-bottom: 2rem">
  <thead>
    <tr>
      <th>Factura</th>
      <th class="mdl-data-table__cell--non-numeric">Cedula</th>
      <th class="mdl-data-table__cell--non-numeric">Razon Social</th>
      <th class="mdl-data-table__cell--non-numeric">Fecha</th>
      <th>Sub. Total</th>
      <th>IVA</th>
      <th>Total</th>
      <th class="mdl-data-table__cell--non-numeric">Accion</th>
    </tr>
  </thead>
  <tbody>
     @if($facturas != '')
      @foreach($facturas as $factura )
      <tr>
        <td>{{ $factura['numero'] }}</td>
        <td>{{ $factura['cedula'] }}</td>
        <td class="mdl-data-table__cell--non-numeric">{{ $factura['razon_social'] }}</td>
        <td class="mdl-data-table__cell--non-numeric">{{ date('d-m-Y H:i:s',strtotime($factura['fecha'])) }}</td>
        <td>{{ number_format($factura['subtotal'], 2, ',', '.') }}</td>
        <td>{{ number_format($factura['iva'], 2, ',', '.') }}</td>
        <td>{{ number_format($factura['total'], 2, ',', '.') }}</td>
        <td class="mdl-data-table__cell--non-numeric">
          <a href="{{ route('ventas.show', $factura['numero']) }}">
            <button class="mdl-button mdl-js-button mdl-button--icon">
              <i class="material-icons">remove_red_eye</i>
            </button>
          </a>
        </td>

      </tr>
      @endforeach
    @endif
  </tbody>
</table>
@endsection