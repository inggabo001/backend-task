@extends('layouts.main')

@section('title','Ventas')
<!-- titulo en el contenido -->
@section('tit')
  <h2>Factura</h2>
@endsection

@section('content')
	
	<table class="mdl-data-table mdl-js-data-table mdl-shadow--2dp" style="margin-bottom: 20px">
		
		<tr>
			<th colspan="5" class="mdl-data-table__cell--non-numeric">
				Factura {{ $ventas[0]->factura_numero }}
			</th>
		</tr>

		<tr>
			<th colspan="5" class="mdl-data-table__cell--non-numeric">
				{{ date('d-m-Y',strtotime($ventas[0]->fecha)) }}
			</th>
		</tr>
		<tr>
			<th colspan="5" class="mdl-data-table__cell--non-numeric">
				{{ date('H:i:s',strtotime($ventas[0]->fecha)) }}
			</th>
		</tr>

		<tr>
			<th colspan="5" class="mdl-data-table__cell--non-numeric">
				Datos del Cliente:
			</th>
		</tr>
		<tr>
			<td colspan="5" class="mdl-data-table__cell--non-numeric" style="border:0px">
				Cedula: {{ $ventas[0]->cedula }}
			</td>
		</tr>
		<tr>
			<td colspan="5" class="mdl-data-table__cell--non-numeric" style="border:0px">
				Razon Social: {{ $ventas[0]->razon_social }}
			</td>
		</tr>
		<tr>
			<td colspan="5" class="mdl-data-table__cell--non-numeric" style="border:0px">
				Telefono: {{ $ventas[0]->telefono }}
			</td>
		</tr>
		<tr>
			<td colspan="5" class="mdl-data-table__cell--non-numeric" style="border:0px">
				Direccion: {{ $ventas[0]->direccion }}
			</td>
		</tr>

		<tr>
			<th class="mdl-data-table__cell--non-numeric">Codigo</th>
			<th class="mdl-data-table__cell--non-numeric">Producto</th>
			<th>Cantidad</th>
			<th>Precio Unitario</th>
			<th>Precio</th>
		</tr>
		<!-- se muestran todos los productos -->
		@foreach($ventas as $venta)
			<tr>
				<td class="mdl-data-table__cell--non-numeric">{{ $venta->codigo }}</td>
				<td class="mdl-data-table__cell--non-numeric">{{ $venta->nombre }}</td>
				<td>{{ $venta->cantidad }}</td>
				<td>{{ number_format($venta->precio_unitario, 2, ',','.') }}</td>
				<td>{{ number_format($venta->precio, 2, ',','.') }}</td>
			</tr>
		@endforeach

		<tr>
			<th colspan="4">Sub-Total</th>
			<th> {{ number_format($subtotal, 2, ',','.') }}</th>
		</tr>

		<tr>
			<th colspan="4">Iva</th>
			<th> {{ number_format($iva, 2, ',','.') }}</th>
		</tr>

		<tr>
			<th colspan="4">Total General</th>
			<th> {{ number_format($total, 2, ',','.') }}</th>
		</tr>
	</table>

@endsection