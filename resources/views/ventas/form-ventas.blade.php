<!-- extendemos a la plantilla principal donde se encuentra el menu-->
@extends('layouts.main')

<!-- titulo de la pagina -->
@section('title','Home')

<!-- titulo en el contenido -->
@section('tit')
  <h2>Registrar Venta</h2>
@endsection

<!-- seccion para mostrar errores de validacion de formularios -->
@section('errors')
 @if ($errors->any())
      <div class="alert alert-danger">
          <ul>
              @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
              @endforeach
          </ul>
      </div>
  @endif
@endsection

<!-- contenido -->
@section('content')
  
<div>
  <!-- divs contenedores de la barra de tabs y enlaces -->
  <div class="mdl-tabs mdl-js-tabs mdl-js-ripple-effect">
  <div class="mdl-tabs__tab-bar">
    <a href="#tab1" class="mdl-tabs__tab">Cliente</a>
    <a href="#tab2" class="mdl-tabs__tab">Productos</a>
  </div>
  <!-- formulario divido en partes utilizando tabs-->
  <form action="{{ route('ventas.store') }}" method="POST">
    <!-- enviar el metodo para definir que ruta usar -->
    <input type="hidden" name="_method" value="POST">
    <!-- enviado el token al backend -->
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <!-- creamos un contador -->
    <!-- seccion para seleccionar el cliente -->
    <div class="mdl-tabs__panel is-active" id="tab1">
      @foreach($clientes as $cliente)
        <div class="mdl-textfield">
          <label class = "mdl-radio mdl-js-radio">
            <input type="radio" name="cliente" class = "mdl-radio__button" value="{{$cliente['cedula']}}" required>
             <span class="mdl-radio__label">
              {{ "Cedula: ".$cliente['cedula'] }}
              <br>
              {{ "Razon Social: ".$cliente['razon-social'] }}
            </span>
          </label>
        </div>
        <br>
      @endforeach
    </div>
    <!-- seccion para seleccionar los productos a vender -->
    <div class="mdl-tabs__panel" id="tab2">
      <!-- si hay productos disponibles -->
      @if(count($productos) > 0)
      <!-- se muestran todos los productos -->
        @foreach($productos as $producto)
        <!-- Si el producto no tiene existencia no lo mostramos -->
          @if($producto->cantidad > 0)
            <div class="mdl-textfield">
              <label class = "mdl-checkbox mdl-js-checkbox">
              <input type="checkbox" class="mdl-checkbox__input" name="productos[]" value="{{$producto->codigo}}">
              <span class = "mdl-checkbox__label">
              {{"Nombre: ".$producto->nombre}}<br>
              {{"Precio: ".number_format($producto->precio, 2 , ",",".")."bs."}}<br>
              </span>
              </label>

            </div>
            <br>
            <div class="mdl-textfield">
              {{"Cantidad: "}} <input type="number" name="cantidad[{{$producto->codigo}}]" min="1" max="{{ $producto->cantidad }}" ><br>
            </div>
            <br>
          @endif

        @endforeach
        <br>
        <button type="submit" value="facturar" class="mdl-button mdl-js-button mdl-button--raised" name="accion">Facturar</button>
        @else
          <br>
            No hay productos en existencia.
      @endif
      
    </div>

  </form>

</div>

@endsection