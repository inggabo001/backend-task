<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>App - @yield('title') </title>
    <!-- material design lite para un dise;o rapido -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="https://code.getmdl.io/1.3.0/material.indigo-pink.min.css">

    <!-- Bootstrap..
        utilizamos bootstrap para los estilos
        de las notificaciones del paquete flash -->
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <!-- stilo -->
    <style>
        .contenido{
            margin-top: 2rem;
            display: flex;
            justify-content: center;
        }

        a{
            text-decoration: none;
        }

        a:hover{
            text-decoration: none;
        }

    </style>
    <!-- section para los estilos en las paginas -->
    @yield('styles')

    
</head>
<body>
    <!-- Always shows a header, even in smaller screens. -->
    <div class="mdl-layout mdl-js-layout mdl-layout--fixed-header">
    <header class="mdl-layout__header">
        <div class="mdl-layout__header-row">
        <!-- Title -->
        <span class="mdl-layout-title">Title</span>
        <!-- Add spacer, to align navigation to the right -->
        
        <div class="mdl-layout-spacer"></div>
        <!-- Navigation. We hide it in small screens. -->
        <nav class="mdl-navigation mdl-layout--large-screen-only">
            <a class="mdl-navigation__link" href="{{ route('ventas.index') }}">Ventas</a>
            <a class="mdl-navigation__link" href="{{ route('clientes.index') }}">Clientes</a>
            <a class="mdl-navigation__link" href="{{ route('productos.index') }}">Productos</a>
        </nav>
        </div>
    </header>
    <div class="mdl-layout__drawer">
        <span class="mdl-layout-title">Title</span>
        <nav class="mdl-navigation">
            <a class="mdl-navigation__link" href="{{ route('ventas.index') }}">Ventas</a>
            <a class="mdl-navigation__link" href="{{ route('clientes.index') }}">Clientes</a>
            <a class="mdl-navigation__link" href="{{ route('productos.index') }}">Productos</a>
        </nav>
    </div>
    <main class="mdl-layout__content">
        <div class="page-content">
            @yield('tit')
            <div class="contenido">
                <!-- Section para desplegar las alertas -->
                @include('flash::message')

                @yield('errors')
            </div>
            <div class="contenido">
                @yield('agregar')
            </div>
            <div class="contenido">
                

                <!-- section para el contenido -->
                @yield('content')
            </div>    

        </div>
    </main>
    </div>
    
    <!-- JavaScript -->
    <!-- js para material design lite -->
    <script defer src="https://code.getmdl.io/1.3.0/material.min.js"></script>
</body>
</html>