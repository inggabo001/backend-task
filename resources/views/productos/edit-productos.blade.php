<!-- extendemos a la plantilla principal donde se encuentra el menu-->
@extends('layouts.main')

<!-- titulo de la pagina -->
@section('title','Editar producto')

<!-- titulo en el contenido -->
@section('tit')
  <h2>Editar producto</h2>
@endsection

<!-- seccion para mostrar errores de validacion de formularios -->
@section('errors')
 @if ($errors->any())
      <div class="alert alert-danger">
          <ul>
              @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
              @endforeach
          </ul>
      </div>
  @endif
@endsection

<!-- contenido -->
@section('content')
 <!-- formulario para edicion de productos-->

<form action="{{ route('productos.update', $producto[0]->codigo) }}" method="POST">
    <input type="hidden" name="_method" value="PUT">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">

    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
        <input class="mdl-textfield__input" type="text" id="codigo" name="codigo" value="{{ $producto[0]->codigo }}">
        <label class="mdl-textfield__label" for="codigo">Codigo</label>
    </div>
    <br>
    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
        <input class="mdl-textfield__input" type="text" id="nombre" name="nombre" value="{{ $producto[0]->nombre }}">
        <label class="mdl-textfield__label" for="nombre">Nombre</label>
    </div>
    <br>
    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
        <input class="mdl-textfield__input" type="text" id="cantidad" name="cantidad" value="{{ $producto[0]->cantidad }}">
        <label class="mdl-textfield__label" for="cantidad">Cantidad</label>
    </div>
    <br>
    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
        <input class="mdl-textfield__input" type="text" id="precio" name="precio" value="{{ $producto[0]->precio }}">
        <label class="mdl-textfield__label" for="precio">Precio</label>
    </div>
    <br>

    <button type="submit" value="Modificar" class="mdl-button mdl-js-button mdl-button--raised" name="Modificar">Modificar</button>

</form>
@endsection
