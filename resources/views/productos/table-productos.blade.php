@extends('layouts.main')

@section('title','Productos')
<!-- titulo en el contenido -->
@section('tit')
  <h2>Productos</h2>
@endsection

@section('agregar')
  <div style="float: right; margin-bottom: 20px;">
  <a href="{{ route('productos.create')}} ">
    <button class="mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect">
    <i class="material-icons">add</i>  
    </button>
  </a>
</div>
@endsection

@section('content')

<table class="mdl-data-table mdl-js-data-table mdl-shadow--2dp">
  <thead>
    <tr>
      <th>Codigo</th>
      <th class="mdl-data-table__cell--non-numeric">Nombre</th>
      <th>Cantidad</th>
      <th>Precio</th>
      <th class="mdl-data-table__cell--non-numeric">Accion</th>
    </tr>
  </thead>
  <tbody>
    <!-- si la tabla clientes no esta vacia hacemos un recorrido
        y mostramos todos los datos -->
    @if($productos != '')
      @foreach($productos as $producto )
      <tr>
        <td>{{ $producto->codigo }}</td>
        <td class="mdl-data-table__cell--non-numeric">{{ $producto->nombre }}</td>
        <td>{{ $producto->cantidad }}</td>
        <td>{{ number_format($producto->precio, 2, ',', '.') }}</td>
        <td class="mdl-data-table__cell--non-numeric">
          <a href="productos/{{ $producto->codigo }}/edit">
            <button class="mdl-button mdl-js-button mdl-button--icon">
              <i class="material-icons">mode_edit</i>
            </button>
          </a>
          
            <a href="{{ route('productos.destroy', $producto->codigo) }}">
              <button class="mdl-button mdl-js-button mdl-button--icon">
                <i class="material-icons">delete</i>
              </button>
          </a>
        </td>

      </tr>
      @endforeach
    @endif
    
  </tbody>
</table>
@endsection