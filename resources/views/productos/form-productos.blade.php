<!-- extendemos a la plantilla principal donde se encuentra el menu-->
@extends('layouts.main')

<!-- titulo de la pagina -->
@section('title','Agregar producto')

<!-- titulo en el contenido -->
@section('tit')
  <h2>Agregar producto</h2>
@endsection

<!-- seccion para mostrar errores de validacion de formularios -->
@section('errors')
 @if ($errors->any())
      <div class="alert alert-danger">
          <ul>
              @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
              @endforeach
          </ul>
      </div>
  @endif
@endsection

<!-- contenido -->
@section('content')
 <!-- formulario para crear clientes con la ruta clientes.store definida en la ruta resources -->
 
<form action="{{ route('productos.store') }}" method="POST">
    <input type="hidden" name="_method" value="POST">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">

    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
        <input class="mdl-textfield__input" type="text" id="codigo" name="codigo">
        <label class="mdl-textfield__label" for="codigo">Codigo</label>
    </div>
    <br>
    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
        <input class="mdl-textfield__input" type="text" id="nombre" name="nombre">
        <label class="mdl-textfield__label" for="nombre">Nombre</label>
    </div>
    <br>
    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
        <input class="mdl-textfield__input" type="text" id="cantidad" name="cantidad">
        <label class="mdl-textfield__label" for="cantidad">Cantidad</label>
    </div>
    <br>
    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
        <input class="mdl-textfield__input" type="text" id="precio" name="precio">
        <label class="mdl-textfield__label" for="precio">Precio</label>
    </div>
    <br>

    <button type="submit" value="Registrar" class="mdl-button mdl-js-button mdl-button--raised" name="Registrar">Registrar</button>

</form>
@endsection
