<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Cliente;

class ClientesController extends Controller
{
    
    public function index()
    {
        /* se obtienen los datos y se retorna a la vista
        donde se muestran todos los clientes.*/
        $clientes= Cliente::all();
        return view('clientes.table-clientes', ['clientes' => $clientes]);
    }

   
    public function create()
    {
        // retorna al formulario para agregar nuevos clientes
        return view('clientes.form-cliente');
    }

  
    public function store(Request $request)
    {
        // validacion de los datos del formulario con request

        $request->validate([
            'cedula' => 'required|max:8|unique:clientes',
            'razon_social' => 'required|max:100',
            'telefono' => 'required|min:12|max:12',
            'direccion' => 'required',
        ]);

        $datos=$request->all();
        // se organizan los datos para el uso de eloquent
        $cliente= [
                    "cedula"    =>  $datos['cedula'],
                    "razon_social"  =>  $datos['razon_social'],
                    "telefono"     =>  $datos['telefono'],
                    "direccion"     =>  $datos['direccion']

        ];
        // se crea el nuevo cliente
        if (Cliente::create($cliente))
        {   
            // alerta a mostrar si la operacion es exitosa
            flash('Registro realizado')->success();
            return redirect('clientes');
        }
        // alerta a mostrar si la operacion falla
        flash('Ocurrio un error inesperado en el registro')->error();
        return redirect('clientes');
    }

    public function edit($cedula)
    {
        /*Se busca la informacion del cliente solicitado
        y se retorna a la vista para modificarlo*/
        $cliente=Cliente::find($cedula);
       
        return view('clientes.edit-cliente', ['cliente' => $cliente->toArray()]);
    }

    
    public function update(Request $request, $cedula)
    {
        //validacion de datos con request
        /*Si la cedula del cliente no se modifico
            no es necesario aplicar el valor unique*/
        if ($cedula == $request->cedula) {
            $request->validate([
                'cedula' => 'required|max:8',
                'razon_social' => 'required|max:100',
                'telefono' => 'required|min:12|max:12',
                'direccion' => 'required',
              ]);
        }
        else{
            /*Si la cedula se modifico verificamos que
            no sea igual a otro existente*/
            $request->validate([
                'cedula' => 'required|max:8|unique:clientes',
                'razon_social' => 'required|max:100',
                'telefono' => 'required|min:12|max:12',
                'direccion' => 'required',
            ]);
        }
       
        
        $datos=$request->all();
        // actualizamos los datos del cliente en la base de datos
        $cliente=Cliente::where('cedula', $cedula)
                    ->update([
                        'cedula' => $datos['cedula'], 
                        'razon_social'  =>  $datos['razon_social'], 
                        'telefono'     =>  $datos['telefono'], 
                        'direccion'     =>  $datos['direccion']
                        ]);
        //Verificamos si se hizo la actualizacion
        if($cliente)
        {
            // alerta a mostrar si la operacion es exitosa
            flash('Modificacion realizada')->success();
            return redirect('clientes');
        }
        // alerta a mostrar si la operacion falla
        flash('Ocurrio un error inesperado en la modificacion')->error();
        return redirect('clientes');
    }

  
    public function destroy($cedula)
    {
        //Buscamos el cliente
        $cliente=Cliente::find($cedula);
        /*Ejecutamos la funcion de eliminar 
        de eloquent para el cliente seleccionado*/
        if($cliente->delete())
        {
            // alerta a mostrar si la operacion es exitosa
            flash('Cliente eliminado')->success();
            return redirect('clientes');
        }
        // alerta a mostrar si la operacion falla
        flash('Ocurrio un error inesperado en la eliminacion')->error();
        return redirect('clientes');
    }
}
