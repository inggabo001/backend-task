<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Venta;
use App\Models\Cliente;
use App\Models\Factura;

class VentasController extends Controller
{
    
    public function index()
    {
        /*se obtienen todos los datos de las ventas pertenecientes
        a las distintas facturas*/
        $ventas=DB::table('ventas')
            ->join('facturas', 'ventas.factura_numero', '=', 'facturas.numero')
            ->join('clientes', 'facturas.cliente_cedula', '=', 'clientes.cedula')
            ->join('productos', 'ventas.producto_codigo', '=', 'productos.codigo')
            ->join('precios', 'productos.codigo', '=', 'precios.producto_codigo')
            ->select('ventas.factura_numero', 'productos.codigo', 'facturas.fecha', 'clientes.cedula', 'clientes.razon_social', 'precios.precio', 'ventas.cantidad')
            ->whereRaw('facturas.fecha >= precios.desde')
            ->where(function ($query) {
                $query->whereRaw('facturas.fecha <= precios.hasta')
                    ->orWhereRaw("precios.status =  'activo'");
            })
            ->get();
        
    
        /*Luego se recorre cada venta para obtener los datos
            necesarios para crear las facturas,
            almacenandolos en un arreglo*/
        /*Se inicializa una variable como punto de comparacion
        entre las facturas..*/
        /*Teniendo en cuenta que cada dato en la tabla ventas,
        representa el producto y la cantidad vendida para 
        ese producto en la factura */
        $fact=0;

        foreach ($ventas as $venta) {
            /* Si el numero de factura es diferente al numero
            de factura guardado anteriormente creamos otra factura..*/
            if ($venta->factura_numero!=$fact) {
                /*si es la primera factura que crearemos inicializamos
                el contador del arreglo de las facturas en 0 */
                if ($fact==0) {
                    $i=0;
                }
                else{
                    /* sino calculamos el iva y el total de la factura
                    lo almacenamos y aumentamos el contador para crear
                    la otra factura...*/
                    $factura[$i]['iva']=$factura[$i]['subtotal'] * 0.12;
                    $factura[$i]['total']=$factura[$i]['subtotal'] + $factura[$i]['iva'];
                    $i++;
                }
                /*creamos la factura nueva*/
                
                $subtotal= $venta->precio * $venta->cantidad;
                $iva= $subtotal * 0.12;
                $total= $subtotal + $iva;

                /*arreglo para las facturas*/
                $factura[$i]=[
                    'numero'        =>  $venta->factura_numero,
                    'cedula'        =>  $venta->cedula,
                    'razon_social'  =>  $venta->razon_social,
                    'fecha'         =>  $venta->fecha,
                    'subtotal'      =>  $subtotal,
                    'iva'           =>  $iva,
                    'total'         =>  $total,
                ];

                $fact=$venta->factura_numero;
            }
            else{
                /*Si la venta pertenece a la misma factura,
                    aumentamos el subtotal, iva y total de la factura.*/
                $sub=$venta->precio * $venta->cantidad;
                
                $factura[$i]['subtotal'] = $factura[$i]['subtotal'] + $sub;
                $factura[$i]['iva']= $factura[$i]['subtotal'] * 0.12;
                $factura[$i]['total']= $factura[$i]['subtotal'] + $factura[$i]['iva'];
            }
        }

        return view('ventas.table-ventas', ['facturas' => $factura]);
    }

    
    public function create()
    {   
        // Seleccion de todos los clientes
        $clientes= Cliente::all();

        /* Adicional se seleccionan los productos con sus precios
        para enviarlos al formulario de realizar una nueva venta.*/

        $productos = DB::table('productos')
            ->join('precios', 'productos.codigo', '=', 'precios.producto_codigo')
            ->select('productos.codigo', 'productos.nombre', 'productos.cantidad', 'precios.precio')
            ->where('precios.status', '=', 'activo')
            ->where('productos.cantidad', '>', 0)
            ->get();
            
        // retornamos a la vista y enviamos los datos
        return view('ventas.form-ventas', ['clientes' => $clientes, 'productos' => $productos]);
    }


    public function store(Request $request)
    {
        /* se validan los datos requeridos*/
        $request->validate([
            'cliente' => 'required',
            'productos' => 'required',
        ]);

        $datos = $request->all();

        $factura=[
                    'fecha' => now(),
                    'cliente_cedula' => $datos['cliente']
        ];

        $productos= $datos['productos'];
        $cantidad=  $datos['cantidad'];
        /* se valida que la cantidad de los productos
        seleccionados sean mayores que 0.
        Si lo es se retorna al formulario */
        foreach ($productos as $producto) {

            if ( $cantidad[$producto] <= 0 ) {
                flash('La cantidad del producto '.$producto.' no puede ser igual o menor a 0.')->error();
                return back()->withInput();
            }

        }
        /* se registra la nueva factura */
        if (Factura::create($factura)) {
            /* se selcciona el codigo de la factura 
            que se acaba de registrar*/
            $fact=DB::table('facturas')
                ->where("fecha", "=", $factura['fecha'])
                ->where("cliente_cedula", "=", $factura['cliente_cedula'])
                ->get();

            for ($i=0; $i < count($productos); $i++) 
            {
                /*se cargan todas las ventas pertenecientes
                a la factura nueva*/
                DB::table('ventas')->insert(
                    ['factura_numero' => $fact[0]->numero, 'producto_codigo'=> $productos[$i], 'cantidad'=>(int)$cantidad[$productos[$i]] ]
                );
                /* Se disminuye la cantidad de los productos en existencia */
                DB::table('productos')->where('codigo', $productos[$i])->decrement('cantidad', (int)$cantidad[$productos[$i]]);
            }

            // alerta a mostrar si la operacion es exitosa
            return redirect()->route('ventas.show',['numero' => $fact[0]->numero]);
        }

    }

   
    public function show($id)
    {
        /* se obtienen todas las ventas pertenecientes a la factura
        indicada con el fin de armar la vista de factura.*/
        $ventas=DB::table('ventas')
            ->join('facturas', 'ventas.factura_numero', '=', 'facturas.numero')
            ->join('clientes', 'facturas.cliente_cedula', '=', 'clientes.cedula')
            ->join('productos', 'ventas.producto_codigo', '=', 'productos.codigo')
            ->join('precios', 'productos.codigo', '=', 'precios.producto_codigo')
            ->select('ventas.factura_numero',  'facturas.fecha', 'clientes.cedula', 'clientes.razon_social', 'clientes.telefono','clientes.direccion', 'productos.codigo', 'productos.nombre','precios.precio as precio_unitario', 'ventas.cantidad', DB::raw('precios.precio * ventas.cantidad as precio'), DB::raw('(precios.precio * ventas.cantidad) * 0.12 as iva') )
            ->where('ventas.factura_numero', '=', $id)
            ->whereRaw('facturas.fecha >= precios.desde')
            ->where(function ($query) {
                $query->whereRaw('facturas.fecha <= precios.hasta')
                    ->orWhereRaw("precios.status =  'activo'");
            })
            ->get();
        /* Variables para calcular el subtotal y el iva*/
        $subtotal= 0;
        $iva= 0;
        /*Se calcula el subtotal y el iva*/
        foreach ($ventas as $venta) {
            
            $subtotal= $subtotal + $venta->precio;
            $iva= $iva + $venta->iva;

        }

        $total= $subtotal + $iva;
        // se muestra la factura.
        return view('ventas.factura', ['ventas' => $ventas, 'subtotal' => $subtotal, 'iva' => $iva, 'total' => $total]);
    }

   
    public function edit($id)
    {
        //
    }


    public function update(Request $request, $id)
    {
        //
    }


    public function destroy($id)
    {
        //
    }
}
