<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Producto;
use App\Models\Precio;

class ProductosController extends Controller
{
  
    public function index()
    {
        /*Se realiza un join para la busqueda
        de los productos con su precio actual
        y se evnia los datos a la vista*/
        $productos = DB::table('productos')
            ->join('precios', 'productos.codigo', '=', 'precios.producto_codigo')
            ->select('productos.*', 'precios.*')
            ->where('precios.status', '=', 'activo')
            ->get();

        return view('productos.table-productos', ['productos' => $productos]);

    }

  
    public function create()
    {
        /*se retorna al formulario que permite
        el registro de nuevos productos*/
         return view('productos.form-productos');
    }

    
    public function store(Request $request)
    {
        /*validamos lo datos del formulario de registro*/
        $request->validate([
            'codigo' => 'required|max:15|unique:productos',
            'nombre' => 'required|max:25',
            'cantidad' => 'required|integer',
            'precio' => 'required|numeric',
        ]);

        $datos=$request->all();
        /*organizamos los datos del producto y sus precios*/
        $producto= [
                    'codigo' => $datos['codigo'],
                    'nombre' => $datos['nombre'],
                    'cantidad' => $datos['cantidad']
        ];

        $precio = [
                   'precio' => $datos['precio'],
                   'desde'  => now(),
                   'producto_codigo' => $datos['codigo'],
                   'status' =>  'activo', 
        ];
        /*creamos el producto nuevo con su respectivo precio*/
        if (Producto::create($producto) && Precio::create($precio))
        {   
            // alerta a mostrar si la operacion es exitosa
            flash('Registro realizado')->success();
            return redirect('productos');
        }
        // alerta a mostrar si la operacion falla
        flash('Ocurrio un error inesperado en el registro')->error();
        return redirect('productos');
    }

    
    public function show($id)
    {
        //
    }

    
    public function edit($codigo)
    {
        //
        $producto = DB::table('productos')
            ->join('precios', 'productos.codigo', '=', 'precios.producto_codigo')
            ->select('productos.*', 'precios.*')
            ->where('precios.status', '=', 'activo')
            ->where('productos.codigo', '=', $codigo)
            ->get();
       
        return view('productos.edit-productos', ['producto' => $producto->toArray()]);

    }

   
    public function update(Request $request, $codigo)
    {
    
        //validacion de datos con request
        if ($codigo == $request->codigo) {
            $request->validate([
                'codigo' => 'required|max:15',
                'nombre' => 'required|max:25',
                'cantidad' => 'required|integer',
                'precio' => 'required|numeric',
            ]);
        }
        else{
            $request->validate([
                'codigo' => 'required|max:15|unique:productos',
                'nombre' => 'required|max:25',
                'cantidad' => 'required|integer',
                'precio' => 'required|numeric',
            ]);
        }
       
        
        $datos=$request->all();
    

        $producto=Producto::where('codigo', $codigo)
                    ->update([
                        'codigo' => $datos['codigo'],
                        'nombre' => $datos['nombre'],
                        'cantidad' => $datos['cantidad']
                        ]);

        $precio=Precio::where('producto_codigo', $codigo)
                    ->where('status','activo')
                    ->update([
                        'hasta'  => now(),
                        'status' => 'inactivo'
                        ]);

        $precio = [
                   'precio' => $datos['precio'],
                   'desde'  => now(),
                   'producto_codigo' => $datos['codigo'],
                   'status' =>  'activo', 
        ];

        if($producto && Precio::create($precio))
        {
            // alerta a mostrar si la operacion es exitosa
            flash('Modificacion realizada')->success();
            return redirect('productos');
        }
        // alerta a mostrar si la operacion falla
        flash('Ocurrio un error inesperado en la modificacion')->error();
        return redirect('productos');
    }

    public function destroy($codigo)
    {
        //Buscamos el cliente
        $producto=Producto::find($codigo);
        // ejecutamos la funcion de eliminar
        if($producto->delete())
        {
            // alerta a mostrar si la operacion es exitosa
            flash('Producto eliminado')->success();
            return redirect('productos');
        }
        // alerta a mostrar si la operacion falla
        flash('Ocurrio un error inesperado en la eliminacion')->error();
        return redirect('productos');


    }
}
