<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Precio extends Model
{
    //


    protected $fillable = ['id', 'precio', 'desde', 'hasta', 'producto_codigo','status'];

    public $timestamps = false;

    public function pruductos()
    {
        return $this->belongsTo('App\Models\Producto');
    }
}
