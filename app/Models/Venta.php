<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Venta extends Model
{
    protected $primaryKey = ['factura_numero', 'producto_codigo'];

    protected $fillable = ['factura_numero', 'producto_codigo', 'cantidad'];

    public $timestamps = false;

    public function pruductos()
    {
        return $this->belongsTo('App\Models\Producto');
    }

    public function facturas()
    {
        return $this->belongsTo('App\Models\Factura');
    }
}
