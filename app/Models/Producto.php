<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Producto extends Model
{
    //
    protected $primaryKey = "codigo";

    protected $fillable = ['codigo', 'nombre', 'cantidad'];

    public function precios()
    {
    	return $this->hasMany('App\Models\Precio');
    }

    public function ventas()
    {
    	return $this->hasMany('App\Models\Venta');
    }
}
