<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    //Primary key modificada
    protected $primaryKey = "cedula";

    protected $fillable = ['cedula', 'razon_social', 'telefono', 'direccion'];

    public function facturas()
    {
    	return $this->hasMany('App\Models\Factura');
    }
}
