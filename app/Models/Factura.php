<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Factura extends Model
{
    //
	protected $primaryKey = "numero";

	protected $fillable = ['numero', 'fecha', 'cliente_cedula'];

    public $timestamps = false;

    public function ventas()
    {
    	return $this->hasMany('App\Models\Venta');
    }

   	public function clientes()
    {
        return $this->belongsTo('App\Models\Cliente');
    }
}
