<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// rutas con el prefijo ventas     /ventas/...
Route::prefix('ventas')->group(function () {

    // visualizar todas las ventas
    Route::get('/', function () {
        return view('ventas.table-ventas');
    });

});

// rutas resources para los diferentes modulos
Route::resource('ventas', 'VentasController');

Route::resource('productos', 'ProductosController');

Route::resource('clientes', 'ClientesController');

// rutas para eliminar elementos desde el boton

Route::get('clientes/{id}/destroy', 
    [
        'uses' => 'ClientesController@destroy',
        'as'   => 'clientes.destroy'
    ]);

Route::get('productos/{id}/destroy', 
    [
        'uses' => 'ProductosController@destroy',
        'as'   => 'productos.destroy'
    ]);

Route::get('ventas/{id}/destroy', 
    [
        'uses' => 'VentasController@destroy',
        'as'   => 'ventas.destroy'
    ]);
